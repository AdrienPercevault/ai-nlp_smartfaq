import pandas as pd
from src.preprocessing.utils_preprocessing  import remove_punctuation, remove_emojis, clean_html


def data_cleaning_preprocessing(path_to_csv):

    mydata = pd.read_csv(path_to_csv, encoding='latin-1')

    if 'Title' in mydata.columns:
        mydata['Title'] = mydata['Title'].apply(clean_html)
        mydata['Title'] = mydata['Title'].apply(remove_punctuation)
        mydata['Title'] = mydata['Title'].apply(remove_emojis)

    if 'Body' in mydata.columns:
        mydata['Body'] = mydata['Body'].apply(clean_html)
        mydata['Body'] = mydata['Body'].apply(remove_punctuation)
        mydata['Body'] = mydata['Body'].apply(remove_emojis)

    return mydata

