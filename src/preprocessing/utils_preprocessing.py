import re
import pandas as pd
from emot.emo_unicode import UNICODE_EMO, EMOTICONS


def remove_punctuation(text):
    """
    Remove punctuation
    """
    #replace every punctuation with a whitespace to keep the words correct
    text = re.sub(r'[^\w\s]',' ',text)
    #remove the successive whitespaces
    _RE_COMBINE_WHITESPACE = re.compile(r"\s+")
    no_punc = _RE_COMBINE_WHITESPACE.sub(" ", text).strip()
    return no_punc


def remove_emojis(text):
    """
    Removing emojis
    """
    sentence = text.split()
    new_sentence = []
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)
    emoticon_pattern = re.compile(u'(' + u'|'.join(k for k in EMOTICONS) + u')')

    #since we have an emoticon ":/" as substing in any url, we need to prevent replacing it 
    url_keep_pattern = re.compile("https?://")
  
    for w in sentence :
        w = emoji_pattern.sub(r'', w)
        if (url_keep_pattern.match(w) is None): #if it's not a url
            w = emoticon_pattern.sub(r'', w)
        new_sentence.append(w)
  
  
    return (" ".join(new_sentence))


def clean_html(raw_html):
    """
    Clean balise HTML 
    """
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def merge_question_answer(df_question: list, df_answer: list, char_number: int) -> list:
    """
    Merging questions and answer in a same string with a number of character selectionned
    """
    merged_question_answer = df_question[["Id", "Title"]].merge(
                             df_answer[["ParentId", "Body"]], left_on='Id', right_on='ParentId')
    merged_question_answer = merged.drop(columns=["ParentId"], axis=1)

    concatened_question_answer = []
    for index, row in merged_question_answer.iterrows():
        row["Title"] = _cut_and_complete_string(row["Title"], char_number)
        row["Body"] = _cut_and_complete_string(row["Body"], char_number)

        concatened_question_answer.append(row["Title"] + row["Body"])    

    return concatened_question_answer


def _cut_and_complete_string(row: str, char_number: int) -> str:
    """
    if the string is longer then number of character pre selected then cut the string
    else complete the string to be at the size of the number of character pre selected
    """
    while len(row) < char_number:
        row = row + " "
    if len(row) > char_number:
        row = row[0:char_number]
    return row
